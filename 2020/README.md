#Plans for fiscal year 2020

In 2020, plans are categorised under the following:

- Content
    - Blogs
        - Monthly article on DZone
        - Monthly article on Dev.to
        - 2 Monthly article on Medium
        - Monthly GitLab blog post aside commissioned content.
    - 
- Community
    - Become CNCF Ambassador
    - Get Google Cloud Certified Architect
    - Become Google Developer Expert
    - Meetups
- Contributions
    - Join the Kubernetes release team, first as Shadow.
    - Become a member of the Kubernetes organization on GitHub.
    - Become a member of the Helm organization
